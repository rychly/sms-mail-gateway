local _M = {}

local smsd_helpers = require('smsd-helpers')
local convert_charsets = require('convert-charsets')
local mail_parser = require('mail-parser')

function _M.print_help(arg)
	io.stderr:write("Usage: " .. arg[0] .. " <MSG_FILE> <MSG_ID> <RCPT_ADDRESS> <PHONE_NO>\n")
	io.stderr:write("Process an email message of particular ID and recepient and send its outline as a SMS message to a given phone number by smsd in smstools3.\n")
end

function _M.main(arg, whitelist_file, outgoing_dir)
	if (#arg < 4 or arg[#arg] == "--help") then
		_M.print_help(arg)
		return 1
	end

	local msg_file = arg[1]
	local msg_id = arg[2]
	local rcpt = arg[3]
	local phone = arg[4]

	-- read and parse file
	local file_in = assert(io.open(msg_file, "r"))
	local linesIteratorStack = mail_parser.IteratorStack:create(file_in:lines())
	local parsed = mail_parser.parse(linesIteratorStack, nil, 'text/.*', 300)
	file_in:close()

	-- process the message
	local body, content_type = parsed:getFirstBody('text/.*')
	if (content_type == 'text/html') then
		-- strip HTML tags
		body = body:gsub('<[^>]+>', '')
	end
	local message = parsed:getFromAddress() .. "\n" .. parsed:getSubject() .. "\n" .. body

	-- convert to ASCII
	message = convert_charsets.convert_charsets(message, 'UTF_8', 'ASCII')

	-- simplify the message and truncate it to a maximal length
	message = message:
		gsub("(\n)>[^\n]*", '%1'):	-- remove quoted lines
		gsub('https?://[%w-_%.%?%$%%:/%+=&]+', 'URL'):	-- hide URLs
		gsub('^%s+', ''):gsub('%s+$', ''):	-- remove heading and trailing spaces
		gsub('[%p%s]*(%p)[%p%s]*', '%1'):	-- remove spaces around punctuation characters and shorten punctuation sequences
		gsub('%s+', ' '):	-- shorten space sequences
		sub(1, 3*140 - 1)	-- how many single SMS messages

	-- send the message
	smsd_helpers.send_message(phone, rcpt, message, outgoing_dir)

	return 0
end

return _M
