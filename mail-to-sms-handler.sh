#!/usr/bin/env lua

-- set LUA_PATH and LUA_CPATH to load also local modules
local prefix_path = '.';
package.path = ('%s/?.lua;%s/?/init.lua;%s'):format(prefix_path, prefix_path, package.path)
package.cpath = ('%s/?.so;%s/?/init.so;%s'):format(prefix_path, prefix_path, package.cpath)

-- set a seed for the pseudo-random generator by the six least significant digits of the system time
math.randomseed(tonumber(tostring(os.time()):reverse():sub(1,6)))

local mail_to_sms_handler = require("mail-to-sms-handler")
local uci = require("uci")
local cfg = uci.cursor()
os.exit(mail_to_sms_handler.main(arg, cfg:get("lua", "sms", "whitelist_file"), cfg:get("lua", "sms", "outgoing_dir")))
